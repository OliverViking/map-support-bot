const boop = require('./boop')
const define = require('./define')
const feedback = require('./feedback')
const hug = require('./hug')
const poll = require('./poll')
const trivia = require('./trivia')

module.exports = {
  commands: {
    'Member Commands': { boop, define, feedback, hug, poll, trivia },
  },
}
