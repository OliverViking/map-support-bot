const getUserByUsername = require('../../utils/getUserByUsername')

async function testCooldown({ bot, message, context }) {
  const roomID = message.rid

  // Send the message
  message = await bot.sendToRoom(`I work!`, roomID)
}

module.exports = {
  description: 'Developer test command',
  help: `${process.env.ROCKETCHAT_PREFIX} testCooldown`,
  requireOneOfRoles: ['admin'],
  cooldown: 60,
  call: testCooldown,
}
