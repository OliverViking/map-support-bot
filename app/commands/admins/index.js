const addAWA = require('./addAWA')
const addMinor = require('./addMinor')
const deleteMinor = require('./deleteMinor')
const unMAP = require('./unMAP')

module.exports = {
  commands: { 'Administrator Commands': { addAWA, unMAP } },
}
