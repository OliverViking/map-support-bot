const { api } = require('@rocket.chat/sdk')

const getUserByUsername = async username => {
  let result = await api.get('users.info', { username })
  if (result.success) {
    return result.user
  } else {
    console.error(result)
  }
}

module.exports = getUserByUsername
