# MAP Support Bot

This is the code for MSC Chat's MSC Bot!

# How to contribute

- Fork the repo
- Clone your Fork
- Create a new branch
- **_REMEMBER TO SET YOUR LOCAL GIT CONFIG TO MAKE ANONYMOUS COMMITS_**
- Make your changes
- Push changes
- Send a merge request
